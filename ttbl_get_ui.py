# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Nov 27 2012)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class main_app_frame
###########################################################################

class main_app_frame ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Unofficial AUT Timetable Getter", pos = wx.DefaultPosition, size = wx.Size( 450,300 ), style = wx.DEFAULT_FRAME_STYLE & ~(wx.RESIZE_BORDER | wx.MAXIMIZE_BOX) )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		self.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOW ) )
		
		app_panel_sizer = wx.BoxSizer( wx.VERTICAL )
		
		self.main_app_panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		main_app_sizer = wx.BoxSizer( wx.VERTICAL )
		
		instructions_horiz_sizer = wx.BoxSizer( wx.HORIZONTAL )
		
		
		instructions_horiz_sizer.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.app_instr_lbl = wx.StaticText( self.main_app_panel, wx.ID_ANY, u"Log in below. If your timetable is successfully processed, you will be able to either save it to an \".ics\" file (for import to iCal, Outlook or Google Calendar) or share it with iOS devices over Wi-Fi.", wx.Point( -1,-1 ), wx.DefaultSize, wx.ALIGN_LEFT )
		self.app_instr_lbl.Wrap( 400 )
		instructions_horiz_sizer.Add( self.app_instr_lbl, 0, wx.ALL, 5 )
		
		
		instructions_horiz_sizer.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		
		main_app_sizer.Add( instructions_horiz_sizer, 0, wx.EXPAND, 5 )
		
		
		main_app_sizer.AddSpacer( ( 0, 5), 0, 0, 5 )
		
		login_horiz_sizer = wx.BoxSizer( wx.HORIZONTAL )
		
		
		login_horiz_sizer.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		login_form_sizer = wx.StaticBoxSizer( wx.StaticBox( self.main_app_panel, wx.ID_ANY, u"Arion Account Details" ), wx.VERTICAL )
		
		self.lbl_arion_username = wx.StaticText( self.main_app_panel, wx.ID_ANY, u"Student ID", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.lbl_arion_username.Wrap( -1 )
		login_form_sizer.Add( self.lbl_arion_username, 0, wx.ALL, 5 )
		
		self.txt_arion_username = wx.TextCtrl( self.main_app_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 150,-1 ), wx.TE_LEFT )
		login_form_sizer.Add( self.txt_arion_username, 0, wx.ALL, 5 )
		
		self.lbl_arion_password = wx.StaticText( self.main_app_panel, wx.ID_ANY, u"Password", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.lbl_arion_password.Wrap( -1 )
		login_form_sizer.Add( self.lbl_arion_password, 0, wx.ALL, 5 )
		
		self.txt_arion_password = wx.TextCtrl( self.main_app_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_LEFT|wx.TE_PASSWORD )
		self.txt_arion_password.SetMinSize( wx.Size( 150,-1 ) )
		
		login_form_sizer.Add( self.txt_arion_password, 0, wx.ALL, 5 )
		
		
		login_form_sizer.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.get_ttbl_btn = wx.Button( self.main_app_panel, wx.ID_ANY, u"Get Timetable", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.get_ttbl_btn.SetDefault() 
		login_form_sizer.Add( self.get_ttbl_btn, 0, wx.ALL|wx.EXPAND, 5 )
		
		
		login_form_sizer.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		
		login_horiz_sizer.Add( login_form_sizer, 0, wx.EXPAND, 5 )
		
		
		login_horiz_sizer.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		
		main_app_sizer.Add( login_horiz_sizer, 1, wx.EXPAND, 5 )
		
		
		self.main_app_panel.SetSizer( main_app_sizer )
		self.main_app_panel.Layout()
		main_app_sizer.Fit( self.main_app_panel )
		app_panel_sizer.Add( self.main_app_panel, 1, wx.EXPAND |wx.ALL, 5 )
		
		
		self.SetSizer( app_panel_sizer )
		self.Layout()
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.get_ttbl_btn.Bind( wx.EVT_BUTTON, self.get_timetable )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def get_timetable( self, event ):
		event.Skip()
	

###########################################################################
## Class ttbl_succ_frame
###########################################################################

class ttbl_succ_frame ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Timetable Export Options", pos = wx.DefaultPosition, size = wx.Size( 420,330 ), style = wx.DEFAULT_FRAME_STYLE & ~(wx.RESIZE_BORDER | wx.MAXIMIZE_BOX)  )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		self.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOW ) )
		
		ttbl_succ_panel_sizer = wx.BoxSizer( wx.VERTICAL )
		
		self.ttbl_succ_main_panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.ttbl_succ_main_panel.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOWTEXT ) )
		self.ttbl_succ_main_panel.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOW ) )
		
		ttbl_succ_main_sizer = wx.BoxSizer( wx.VERTICAL )
		
		ttbl_succ_info_horiz_sizer = wx.BoxSizer( wx.HORIZONTAL )
		
		
		ttbl_succ_info_horiz_sizer.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.ttbl_succ_info_lbl = wx.StaticText( self.ttbl_succ_main_panel, wx.ID_ANY, u"Your timetable was successfully downloaded! If you are on a home network (i.e. *not* a public wifi), you can share the file directly to your iPhone/iPad for import. You can also save the file and upload it to Google Calendar, or manually import it into your computer's calendar app.", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.ttbl_succ_info_lbl.Wrap( 400 )
		ttbl_succ_info_horiz_sizer.Add( self.ttbl_succ_info_lbl, 0, wx.ALL, 5 )
		
		
		ttbl_succ_info_horiz_sizer.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		
		ttbl_succ_main_sizer.Add( ttbl_succ_info_horiz_sizer, 0, wx.EXPAND, 5 )
		
		
		ttbl_succ_main_sizer.AddSpacer( ( 0, 30), 0, wx.EXPAND, 5 )
		
		ttbl_succ_options_horiz_sizer = wx.BoxSizer( wx.HORIZONTAL )
		
		
		ttbl_succ_options_horiz_sizer.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		ttbl_succ_options_sizer = wx.BoxSizer( wx.VERTICAL )
		
		self.ttbl_save_btn = wx.Button( self.ttbl_succ_main_panel, wx.ID_ANY, u"Save timetable (*.ics format)", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.ttbl_save_btn.SetDefault() 
		ttbl_succ_options_sizer.Add( self.ttbl_save_btn, 0, wx.ALL|wx.EXPAND, 5 )
		
		
		ttbl_succ_options_sizer.AddSpacer( ( 0, 30), 0, wx.EXPAND, 5 )
		
		self.ttbl_netshare_button = wx.Button( self.ttbl_succ_main_panel, wx.ID_ANY, u"Share over network", wx.DefaultPosition, wx.DefaultSize, 0 )
		ttbl_succ_options_sizer.Add( self.ttbl_netshare_button, 0, wx.ALL|wx.EXPAND, 5 )
		
		
		ttbl_succ_options_sizer.AddSpacer( ( 0, 30), 0, 0, 5 )
		
		self.ttbl_succ_exit_button = wx.Button( self.ttbl_succ_main_panel, wx.ID_ANY, u"Quit", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.ttbl_succ_exit_button.SetForegroundColour( wx.Colour( 255, 0, 0 ) )
		
		ttbl_succ_options_sizer.Add( self.ttbl_succ_exit_button, 0, wx.ALL|wx.EXPAND, 5 )
		
		
		ttbl_succ_options_horiz_sizer.Add( ttbl_succ_options_sizer, 0, wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		
		ttbl_succ_options_horiz_sizer.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		
		ttbl_succ_main_sizer.Add( ttbl_succ_options_horiz_sizer, 1, wx.EXPAND, 5 )
		
		
		self.ttbl_succ_main_panel.SetSizer( ttbl_succ_main_sizer )
		self.ttbl_succ_main_panel.Layout()
		ttbl_succ_main_sizer.Fit( self.ttbl_succ_main_panel )
		ttbl_succ_panel_sizer.Add( self.ttbl_succ_main_panel, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.ttbl_netshare_panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.ttbl_netshare_panel.Hide()
		
		ttbl_netshare_sizer = wx.BoxSizer( wx.VERTICAL )
		
		ttbl_netshare_info_sizer = wx.BoxSizer( wx.HORIZONTAL )
		
		
		ttbl_netshare_info_sizer.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.ttbl_netshare_info_lbl = wx.StaticText( self.ttbl_netshare_panel, wx.ID_ANY, u"Your timetable is now available over your local network. Type the address given below into Safari on your iOS device, and you should be able to import your classes. Third party Android apps may allow you to import the ics file as well.", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.ttbl_netshare_info_lbl.Wrap( 400 )
		ttbl_netshare_info_sizer.Add( self.ttbl_netshare_info_lbl, 0, wx.ALL, 5 )
		
		
		ttbl_netshare_info_sizer.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		
		ttbl_netshare_sizer.Add( ttbl_netshare_info_sizer, 0, wx.EXPAND, 5 )
		
		
		ttbl_netshare_sizer.AddSpacer( ( 0, 40), 0, 0, 5 )
		
		ttbl_netshare_addr_sizer = wx.BoxSizer( wx.HORIZONTAL )
		
		
		ttbl_netshare_addr_sizer.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.ttbl_netshare_addr_label = wx.StaticText( self.ttbl_netshare_panel, wx.ID_ANY, u"Address (type in on phone/tablet):", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.ttbl_netshare_addr_label.Wrap( -1 )
		ttbl_netshare_addr_sizer.Add( self.ttbl_netshare_addr_label, 0, wx.ALL, 5 )
		
		
		ttbl_netshare_addr_sizer.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		
		ttbl_netshare_sizer.Add( ttbl_netshare_addr_sizer, 0, wx.EXPAND, 5 )
		
		self.ttbl_netshare_addr_txtbox = wx.TextCtrl( self.ttbl_netshare_panel, wx.ID_ANY, u"Loading...", wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		ttbl_netshare_sizer.Add( self.ttbl_netshare_addr_txtbox, 0, wx.ALL|wx.EXPAND, 5 )
		
		
		ttbl_netshare_sizer.AddSpacer( ( 0, 40), 0, wx.EXPAND, 5 )
		
		ttbl_netshare_status_sizer = wx.BoxSizer( wx.HORIZONTAL )
		
		self.ttbl_netshare_status_lbl_static = wx.StaticText( self.ttbl_netshare_panel, wx.ID_ANY, u"Status:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.ttbl_netshare_status_lbl_static.Wrap( -1 )
		ttbl_netshare_status_sizer.Add( self.ttbl_netshare_status_lbl_static, 0, wx.ALL, 5 )
		
		
		ttbl_netshare_status_sizer.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.ttbl_netshare_status_info_lbl = wx.StaticText( self.ttbl_netshare_panel, wx.ID_ANY, u"Unknown", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.ttbl_netshare_status_info_lbl.Wrap( -1 )
		self.ttbl_netshare_status_info_lbl.SetForegroundColour( wx.Colour( 255, 0, 0 ) )
		
		ttbl_netshare_status_sizer.Add( self.ttbl_netshare_status_info_lbl, 0, wx.ALL, 5 )
		
		
		ttbl_netshare_sizer.Add( ttbl_netshare_status_sizer, 0, wx.EXPAND, 5 )
		
		
		ttbl_netshare_sizer.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		bSizer14 = wx.BoxSizer( wx.HORIZONTAL )
		
		
		bSizer14.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.ttbl_netshare_stop = wx.Button( self.ttbl_netshare_panel, wx.ID_ANY, u"Stop sharing (return to options)", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer14.Add( self.ttbl_netshare_stop, 0, wx.ALL, 5 )
		
		
		ttbl_netshare_sizer.Add( bSizer14, 0, wx.EXPAND, 5 )
		
		
		self.ttbl_netshare_panel.SetSizer( ttbl_netshare_sizer )
		self.ttbl_netshare_panel.Layout()
		ttbl_netshare_sizer.Fit( self.ttbl_netshare_panel )
		ttbl_succ_panel_sizer.Add( self.ttbl_netshare_panel, 1, wx.EXPAND |wx.ALL, 5 )
		
		
		self.SetSizer( ttbl_succ_panel_sizer )
		self.Layout()
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.ttbl_save_btn.Bind( wx.EVT_BUTTON, self.save_ttbl )
		self.ttbl_netshare_button.Bind( wx.EVT_BUTTON, self.setup_netshare )
		self.ttbl_succ_exit_button.Bind( wx.EVT_BUTTON, self.onclick_exit_btn )
		self.ttbl_netshare_stop.Bind( wx.EVT_BUTTON, self.stop_netshare )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def save_ttbl( self, event ):
		event.Skip()
	
	def setup_netshare( self, event ):
		event.Skip()
	
	def onclick_exit_btn( self, event ):
		event.Skip()
	
	def stop_netshare( self, event ):
		event.Skip()
	

