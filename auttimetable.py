# Dependencies:
# autarion - included
# the `icalendar' module - can be installed with `pip install icalendar' if you have pip
# the lxml module - as above, can be installed with pip

import sys, os, getpass
import re
import autarion
import lxml.html as etree
import urllib2, urllib
import hashlib
import icalendar
import datetime
import uuid
import calendar
from icalendar import Calendar, Event, vDatetime

class AUTTimetableProcessor:
	def __init__(self, autarion_obj):
		self.timetableUrl = 'https://arion.aut.ac.nz/ArionMain/StudentInfo/AcademicDetails/Timetable.aspx'
		self.arion = autarion_obj
		
	#
	# Loads and coordinates the parsing of the text timetable view.
	# Slightly riskier/more fragile than the graphical parser, but enabled
	# access to the full year's calendar in one pull, rather than week- or day-
	# sized blocks.
	#
	# Will simply save the result directly to a 'timetable.ics' file in the working dir.
	# This is temporary behaviour.
	#
	def parse_timetable_text(self):
		self.arion.fetchUrl(self.timetableUrl)
		self.load_calendar_textview()
		appts_flat = list() # Unordered list of Event objects (to be analysed for repeat patterns later... Not implemented)
		tree = self.arion.currentpagetree
		months = tree.forms[0].xpath('//div[@id="wucStudentTimetable_pnlTimetableText"]/table[1]/tr[4]/td[1]/table')
		
		for tbl in months:
			currmonth = tbl.xpath('tr[1]/td[1]')[0].text
			rows = tbl.xpath('tr')
			del rows[0] # Remove month name header
			
			for row in rows:
				appts_flat.append(self.parse_appt_text(currmonth, row))
		
		# Create and populate new iCalendar
		cal = Calendar()
		cal["X-WR-CALNAME"] = "AUT Timetable"
		for appt in appts_flat:
			cal.add_component(appt)
		
		return cal.to_ical()
	
	#
	# Given a month name and row containing textual class information,
	# generates a specific calendar event in iCal format.
	#
	# Returns an Event object of the icalendar library.
	#
	def parse_appt_text(self, monthstr, appt):
		# Get the values from the element
		location = "%s (%s)" % (appt.xpath('td[6]')[0].text.strip(), appt.xpath('td[7]')[0].text.strip())
		course = "%s (%s)" % (appt.xpath('td[9]')[0].text.strip(), appt.xpath('td[8]')[0].text.strip())
		day = int(appt.xpath('td[2]')[0].text.strip())
		start = appt.xpath('td[4]')[0].text.strip()
		end = appt.xpath('td[5]')[0].text.strip()
		
		# Determine start time as two integers
		is_morning = 'a' in start
		start = re.sub(r"[a-zA-Z .]", "", start).split(":")
		start[0] = int(start[0])
		start[1] = int(start[1])
		if not is_morning:
			start[0] = start[0]+12 if start[0] < 12 else 12
		
		# Determine end time as two integers
		is_morning = 'a' in end
		end = re.sub(r"[a-zA-Z .]", "", end).split(":")
		end[0] = int(end[0])
		end[1] = int(end[1])
		if not is_morning:
			end[0] = end[0]+12 if end[0] < 12 else 12
		
		# Generate start datetime
		month = self.string_to_month(monthstr)
		year = int(datetime.datetime.now().year) # As with getting text ttbl.. Will need to be changed
		hour = start[0]
		minute = start[1]
		startdatetime = datetime.datetime(year, month, day, hour, minute)
		
		# Generate end datetime
		hour = end[0]
		minute = end[1]
		enddatetime = datetime.datetime(year, month, day, hour, minute)
		
		# Create the UID
		uid = str(uuid.uuid4())
		
		# Parse the values into iCal event
		thisEvent = Event()
		
		# Add values to the actual iCal event
		thisEvent.add('dtstart', startdatetime)
		thisEvent.add('dtend', enddatetime)
		thisEvent.add('dtstamp', datetime.datetime.now())
		thisEvent.add('uid', uid)
		thisEvent.add('description', '')
		thisEvent.add('location', location)
		thisEvent.add('sequence', '1')
		thisEvent.add('status', 'CONFIRMED')
		thisEvent.add('summary', course)
		thisEvent.add('transp', 'OPAQUE')
	
		return thisEvent
		
	
	#
	# Converts a month's name (given as a string) to an integer between 1-12
	#
	# Returns an integer representation of the given month.
	#
	def string_to_month(self, monthstr):
		mth = 0
		monthreverse = dict((v,k) for k,v in enumerate(calendar.month_abbr))
		del monthreverse[''] # Remove irritating empty string...
		for m,d in monthreverse.iteritems():
			if m.lower() in monthstr.lower():
				mth = d
				break
		if mth == 0:
			return None
			
		return mth
	
	#
	# Finds simple repeat patterns in groups of events and adds repeat properties
	# to the calendar to minimise the size and complexity.
	#
	# Returns a list of iCal events, simplified with recurrence properties set.
	#
	def find_repeats(self, events):
		#newevents = list()
		pass
	
	#
	# Handles the loading of the text view of the calendar (simulates click
	# on 'Text View' link) and handles loading of the new page.
	#
	# No return values.
	#
	def load_calendar_textview(self):
		data = dict() # Dictionary for POST data
		form = self.arion.currentpagetree.forms[0]
		
		# Scrape required fields from form
		fields = [
			"__VIEWSTATE",
			"__EVENTVALIDATION",
			]
		
		for f in fields:
			try:
				data[f] = form.xpath('input[@id="%s"][1]' % f)[0].get("value")
			except:
				print "Could not find field: " + f
		
		# Fill in remainder of required fields
		data["wucStudentTimetable:wucSearch:cscAcademicYear"] = str(datetime.datetime.now().year)
		data["__EVENTARGUMENT"] = ""
		data["__EVENTTARGET"] = "wucStudentTimetable:lcmdTimetableText"
		
		# Reload calendar view + POST form to server
		self.arion.fetchUrl(self.timetableUrl, urllib.urlencode(data))
	#
	# Given an htmlElement (of the lxml library), generates a string containing
	# its relevant data - useful for logging.
	#
	# Returns the string describing the element.
	#
	def stringify_html_element(self, e):
		ks = e.keys()
		desc = ""
		for k in ks:
			desc += "[%s]: %s\n" % (k, e.get(k))
		return desc
		
	#
	# Exit the program, giving an appropriate term signal.
	#
	def exit(self, sig):
		self.log.writeToFile()
		sys.exit(sig)

if __name__ == "__main__":
	tt = Timetable()