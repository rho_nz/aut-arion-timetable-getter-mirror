import urllib, urllib2
import cookielib
import lxml.html as etree
from io import BytesIO

class AUTArion:
	def __init__(self, username, password):
		self.username = username
		self.password = password
		self.cookies = None
		self.opener = None
		self.authenticated = False
		self.loginUrl = "https://arion.aut.ac.nz/ArionMain/Default.aspx"
		self.currentUrl = ""
		self.currentuser = "nobody"
		self.currentpagetree = None
	
	#
	# Fetches a URL with either a GET or POST request (as appropriate).
	# Automatically parses it into an lxml etree for further processing.
	#
	# Returns the response object from the request for reading of any error codes.
	# The etree can be accessed as a property of the AUTARION instance.
	#
	def fetchUrl(self, url, params=None):
		if not self.opener or not self.cookies:
			self.cookies = cookielib.CookieJar()
			self.opener = urllib2.build_opener(urllib2.HTTPHandler(), urllib2.HTTPRedirectHandler(), urllib2.HTTPCookieProcessor(self.cookies))
		resp = self.opener.open(url, data=params)
		page = resp.read()
		self.currentpagetree = etree.fromstring(page)
		return resp
	#
	# Handles login to the ARION system. Returns True on success, False on failure.
	#
	def login(self):
		self.fetchUrl(self.loginUrl)
		# Get the required values for login
		form = self.currentpagetree.forms[0]
		eventvalidation = form.xpath("input[@id=\"__EVENTVALIDATION\"][1]")[0].get("value")
		viewstate = form.xpath("input[@id=\"__VIEWSTATE\"][1]")[0].get("value")
		eventarg = eventtarg = ""
		
		# Prepare POST data
		data = dict()
		data["__EVENTVALIDATION"] = eventvalidation
		data["__VIEWSTATE"] = viewstate
		data["__EVENTARGUMENT"] = eventarg
		data["__EVENTTARGET"] = eventtarg
		data["wucEntry:txtUsername"] = self.username
		data["wucEntry:txtPassword"] = self.password
		data["wucEntry:cmdLogin"] = ""
		
		# Send request and fetch page
		test = self.fetchUrl(self.loginUrl, urllib.urlencode(data))
		
		# Check if successful
		if test.geturl() == self.loginUrl:
			return False
		self.currentuser = self.username
		self.authenticated = True
		return True