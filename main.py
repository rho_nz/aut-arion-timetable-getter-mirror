import sys, os
import ttbl_get_ui as ariongui
from autarion import AUTArion
from auttimetable import AUTTimetableProcessor
import wx
import wx.xrc

import socket, time
from threading import Event, Thread

#
# Very simple HTTP server that simply returns the given string (ical_data) on receiving any GET request 
#
class WebServer:
	def __init__(self, ical_data):
		self.data = ical_data
		self.server_thread = None
		self.server_stop_evt = None
		self.bind_addr = self._get_addr_info()
	
	#
	# Finds a non-localhost IP address and returns it in the appropriate format for socket.bind(addr)
	# *Does NOT handle multiple NICs at the point*
	#
	def _get_addr_info(self):
		return (socket.gethostbyname(socket.getfqdn()), 50505)
	
	#
	# Tells the server thread to terminate
	#
	def stop_server(self):
		self.server_stop_evt.set()
		return True
	
	#
	# Sets up and starts the server thread
	#
	def start_server(self):
		# Create thread and stop event
		
		if self.server_stop_evt is not None:
			self.server_stop_evt.clear()
		else:
			self.server_stop_evt = Event()
			
		if self.server_thread is None or not self.server_thread.isAlive():
			self.server_thread = Thread(target=self.ws_thread_target, args=(self.server_stop_evt, self.bind_addr, self.data))
			# Daemonise and start thread
			self.server_thread.daemon = True
			self.server_thread.start()
			
		return True
	
	#
	# Target function for the server thread
	#
	def ws_thread_target(self, stopevt, bind_addr, data):
		serversock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		serversock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		serversock.bind(bind_addr)
		ical_data = data
		
		while not stopevt.is_set():
			# Block and wait for connections
			serversock.listen(3)
			
			# Handle new request
			conn, addr = serversock.accept()
		
			# Read request data
			data = conn.recv(1024)
			string = bytes.decode(data)
		
			# If it's not a GET, we don't care!
			http_method = string.split(" ")[0]
		
			if http_method.lower().strip() != "get":
				continue
						
			# Prepare headers
			resp_headers = "HTTP/1.1 200 OK\n"
			resp_headers += "Date: " + time.strftime("%a, %d %b %Y %H:%M:%S", time.localtime()) + "\n"
			resp_headers += "Server: AUT-Unofficial-Timetable-Getter-Py\n"
			resp_headers += "Content-Type: text/calendar\n"
			resp_headers += "Connection: close\n\n"
		
			# Prepare server response
			response = resp_headers + ical_data
		
			# Send response and move to next request
			conn.send(response)
			conn.close()
		
		# We are done - close the socket
		serversock.close()
		
#
# Subclass of generated ttbl_succ_frame class - overrides event handler stubs
#
class ArionSuccWindow (ariongui.ttbl_succ_frame):
	def __init__(self, parent, ical_data):
		ariongui.ttbl_succ_frame.__init__(self, parent)
		self.parent = parent
		self.webserver = None
		self.ical_data = ical_data
		self.Show()
	
	#
	# Swaps the options panel out for the network sharing panel, and starts the web server
	#
	def setup_netshare(self, event):
		self.ttbl_succ_main_panel.Hide()
		self.ttbl_netshare_panel.Show()
		self.Fit()
		self.start_netshare()
	#
	# Starts the webserver and sets relevant fields in the network sharing UI
	#
	def start_netshare(self, event=None):
		if self.webserver is None:
			self.webserver = WebServer(self.ical_data)
			
		(addr, port) = self.webserver.bind_addr
		filename = "timetable.ics" # no need to allow user customisation of this...
		addr_string = "http://%s:%s/%s" % (addr, port, filename)
		self.ttbl_netshare_addr_txtbox.SetValue(addr_string)
		
		if self.webserver.start_server():		
			self.ttbl_netshare_status_info_lbl.SetForegroundColour(wx.Colour( 0, 255, 0 ))
			self.ttbl_netshare_status_info_lbl.SetLabel("Enabled")
		else:
			self.ttbl_netshare_status_info_lbl.SetForegroundColour(wx.Colour( 255, 0, 0 ))
			self.ttbl_netshare_status_info_lbl.SetLabel("Unknown")
	#
	# Stops network sharing and switches back to the options panel
	#
	def stop_netshare(self, event):
		self.webserver.stop_server()	
		self.ttbl_netshare_status_info_lbl.SetForegroundColour(wx.Colour( 255, 0, 0 ))
		self.ttbl_netshare_status_info_lbl.SetLabel("Disabled")
		
		self.ttbl_netshare_panel.Hide()
		self.ttbl_succ_main_panel.Show()
		self.Fit()
	
	#
	# Displays a "Save" dialog and handles writing of the calendar file at the selected location
	#
	def save_ttbl(self, event):
		savedlg = wx.FileDialog(self, "Save timetable as...", os.getcwd(), "My_AUT_Timetable", "*.ics", wx.SAVE|wx.OVERWRITE_PROMPT)
		result = savedlg.ShowModal()
		inFile = savedlg.GetPath()
		savedlg.Destroy()

		if result == wx.ID_OK:          #Save button was pressed
			f = open(inFile, 'w')
			f.write(self.ical_data)
			f.close()
			return True
		elif result == wx.ID_CANCEL:    #Either the cancel button was pressed or the window was closed
			return False
	
	#
	# Handles exit/quit button click (terminates app)
	# 
	def onclick_exit_btn(self, event):
		warn = wx.MessageDialog(self, "Are you sure you want to quit?", "Confirm Exit", wx.OK|wx.CANCEL|wx.ICON_QUESTION)
		result = warn.ShowModal()
		warn.Destroy()
		if result == wx.ID_OK:
			self.parent.Destroy()
#
# Subclass of main_app_frame - implements get_timetable event handler
#
class ArionMainWindow (ariongui.main_app_frame):
	def __init__(self, parent):
		ariongui.main_app_frame.__init__(self, parent) # run parent constructor
		
	def get_timetable(self, event):
		username = self.txt_arion_username.GetValue()
		password = self.txt_arion_password.GetValue()
		
		arion = AUTArion(username, password)
		if arion.login():
			ttbl = AUTTimetableProcessor(arion)
			ical = ttbl.parse_timetable_text()
			success_wind = ArionSuccWindow(self, ical)
			self.Hide()
		else:
			wx.MessageBox('Login failed! Check your credentials and try again.', 'Error!', wx.OK | wx.ICON_ERROR)
			
			

if __name__ == "__main__":
	# Construct WX app
	app = wx.App()
	
	# Set up login UI and show
	mainui_frm = ArionMainWindow(None)
	mainui_frm.Show(True)
	
	# Start app
	app.MainLoop()